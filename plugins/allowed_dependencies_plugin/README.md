Allowed Dependencies Plugin
===

Reads .dep and .mem files in a project to check for allowed dependencies are being matched with the dependency rules.
