Lakosian Test Rules Plugin
===

Tools that helps visualize and inspect if Lakosian test dependency rules are being followed.
For more information check John Lakos' books on the topic.
